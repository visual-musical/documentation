# Visual Musical



Project and goals :
----------------------

Visual Musical is a project carried out in a school setting to introduce us to the IoT Domain.
This project is rather artistic since it allows to interpret different colors and create a music based on those colors.
Indeed each color is associated with a frequency that will be played by the « ESP8266 ».
A web application allow to film and capture images, then the colors are converted by the API to frequencies and sent to the Arduino.

Equipments :
-------------------

For this project we used :

- Wemos d1 mini : https://www.amazon.fr/s?k=wemos+d1+mini&adgrpid=57861652282&hvadid=275452725042&hvdev=c&hvlocphy=9056144&hvnetw=g&hvpos=1t1&hvqmt=b&hvrand=8633321591090158083&hvtargid=kwd-329468039204&tag=googhydr0a8-21&ref=pd_sl_68unde6081_b
- Buzzer : https://www.amazon.fr/3-3V-5V-Onboard-Active-Buzzer-Arduino/dp/B00LGX79MM


Architecture :
-------------------

  - Plug your Wemos card to your computer
  ![Image of iot schema](pictures/iot-schema.png)
  ![Picture of Arduino and buzzer](pictures/picture.jpg)

Content :
--------------------------

![Image of architecture schema](pictures/architecture.png)

#### Arduino :

Our Arduino use two libraries. 

- Tone that permit us to transmit to the Wemos a frequecy that will be played on the buzzer.
- Websocket that permit us to communicate with the Wemos with a lighter protocol thant REST. Efficiency is important for us because a we miss a tone it might not render the music that is supposed to be played.

Our API permit us  

##### Required Library :

- **WebSocket**
- **Tone**

#### WEB APP

Our Web application  is only compatible with Google Chrome for now and do three major things.

- First of all it enable the user to use our interface to film with any camera of the device that he is using.
- Then It capture the stream of the video camera and take a picture each 200ms.
- Finally this picture is processed through a canvas and we extract the predominant color.

The colors are then sent to our API that will dispatch it to the device selected by the user.

![Web app example](pictures/web-app-example.png)

##### Required Library :
- **axios (http requester)**
- **react (main framework)**
- We used this sample of code to detect the average color of a canvas https://stackoverflow.com/questions/2541481/get-average-color-of-image-via-javascript

#### API :
Our API is mainly composed of three routes :
- `GET : /devices/:id/color` : send color capture by the web application in 'rgb' format to a given Arduino device (with the id)
- `GET : /devices/list` : get the list of all Arduino devices available
- `GET : /melodies/:kind` : get the melodie of the given track sound displayed with colors on an html page

![Web app example](pictures/partition.png)

Also, the API uses web sockets to send frequencies associated to the colors and their durations to the Arduino.

It also has some intern functions to help us in our work :
- A function to associate a frequency to existing notes.
- A function to associate ranges of colors to notes.
- A function to associate notes to colors.
- A function to return melodies prerecorded in our server.

##### Required Library :

- **Express**
- **WS (WebSocket)**

Step By Step :
--------------------------


Run the Arduino :

- Download Arduino software : https://www.arduino.cc/en/main/software
- You can find Wemos drivers for Windows and MacOS here : https://wiki.wemos.cc/downloads

Setup the board :

- Start Arduino software and open Preferences window.
- Enter `https://arduino.esp8266.com/stable/package_esp8266com_index.json` into `Additional Board Manager` URLs field. (You can add multiple URLs, separating them with commas)
- Open `Boards Manager` from `Tools > Board` menu and install esp8266 platform 
- Select your ESP8266 board from `Tools > Board` menu after installation.
- Don't forget to put your Wifi credentials in the Arduino code
> char ssid[] = "Wifi";
> char pass[] = "Password";

Run the API step :

- Execute `npm install`
- After that you can run the server with the command `node index.js`

Run the web application :

- Execute `npm install`
- After that, just execute `npm start`
- Open Google Chrome and go to the dedicated URL (`http://localhost:80` or  `http://ipaddress:80`)
- When the camera is on, select the device of your choice.
- Finally click on the `Start` button and the web app will start working.


Tech
--------------------------

Dillinger uses a number of open source projects to work properly:

* [ReactJS](https://reactjs.org/) - HTML enhanced for web apps!
* [Node.js](https://nodejs.org/) - evented I/O for the backend
